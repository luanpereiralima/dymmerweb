$( document ).ready(function() {
	getAtividadesParaHtml();
});

function getAtividadesParaHtml(){
	$("#tarefas").html("");
	$.get({url: "tarefa", dataType: 'json' }, function(tarefas)	{	
		console.log(tarefas);
		if(tarefas!=undefined){
			for(var i=0; i < tarefas.length; i++){
				$("#tarefas").html($("#tarefas").html()+
				'<a href="#" class="list-group-item">'+
				    '<h4 class="list-group-item-heading">'+tarefas[i].nome+'</h4>'+
			    	'<p class="list-group-item-text">'+tarefas[i].descricao+'</p>'+
			        '<div class="btn-group" role="group" aria-label="...">'+
			        	'<button type="button" class="btn btn-default" onclick="deletarTarefa(\''+tarefas[i].nome+'\')">Deletar</button>'+
			        	'<button type="button" class="btn btn-default" onclick="editarTarefaMostrarFormulario(\''+tarefas[i].nome+'\', \''+tarefas[i].descricao+'\')">Editar</button>'+
			        '</div>'+
				'</a>');
			}
			if(tarefas.length==0)
				$("#tarefas").html("<h2>Sem Tarefas</h2>");
		}
	});
}	

function deletarTarefa(nome) {
	$.ajax({
	    url: 'tarefa?' + $.param({"nome":nome}),
	    type: 'DELETE',
	    contentType:'application/json',
	    dataType: 'text',            
	    success: function(result) {
	    	window.location.replace("index.html");
	    },
	    error: function(result){
	    	console.log("ERRO");
	    }
	});
}

function editarTarefaMostrarFormulario(nome, descricao) {
	$("#nomeEdit").val(nome);
	$("#descricaoEdit").val(descricao);
	$("#nomeAntigoEdit").val(nome);
	$('#editTarefa').modal('show'); 
}


function editarTarefa() {
	var nomeAntigoEdit = $("#nomeAntigoEdit").val();
	var nomeEdit = $("#nomeEdit").val();
	var descricaoEdit = $("#descricaoEdit").val();
	
	$.ajax({
	    url: 'tarefa?' + $.param({"nome":nomeAntigoEdit, "novoNome":nomeEdit, "novaDescricao":descricaoEdit}),
	    type: 'PUT',
	    contentType:'application/json',
	    dataType: 'text',            
	    success: function(result) {
	    	window.location.replace("index.html");
	    },
	    error: function(result){
	    	console.log("ERRO");
	    }
	});
}
