package ufc.br;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.tree.DefaultMutableTreeNode;

import com.google.gson.Gson;

import br.ufc.lps.controller.xml.ControladorXml;
import br.ufc.lps.model.context.ContextModel;
import br.ufc.lps.model.context.SplotContextModel;
import br.ufc.lps.model.contextaware.Context;
import br.ufc.lps.repository.SchemeXml;
import br.ufc.lps.splar.core.fm.FeatureModel;
import ufc.br.tree.ChildrenTV;
import ufc.br.tree.ConfigTV;
import ufc.br.tree.MakeTreeView;

/**
 * Servlet implementation class ServletTeste
 */
@WebServlet("/treeSchemeXml")
public class ServletTreeSchemeXml extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ContextModel model;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletTreeSchemeXml() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String id = request.getParameter("id");
		String type = request.getParameter("type");
		
		ServletContext contexto = getServletContext();
		
		List<SchemeXml> lista = (List<SchemeXml>)contexto.getAttribute("listaItens");
		
		if(lista!=null){
			for(SchemeXml sc : lista){
				if(sc.get_id().equals(id)){
					File file = ControladorXml.createFileFromXml(sc.getXml());
					model = new SplotContextModel(file.getAbsolutePath());
					break;
				}
			}
		}
		
		if(model!=null){
			Context context = model.getContexts().get("default");
			FeatureModel featureModel = model.setFeatureModel(context);
			
			System.out.println("{\"nameXml\":\""+model.getModelName()+"\",\"tree\":"+MakeTreeView.makeTree(featureModel.getRoot())+"}");
	
			
			if(type!=null && type.equals("1")){
				response.getWriter().append("{\"nameXml\":\""+model.getModelName()+"\",\"tree\":"+MakeTreeView.makeTree(featureModel.getRoot())+"}");
			}else{
				response.getWriter().append("{\"nameXml\":\""+model.getModelName()+"\",\"tree\":"+MakeTreeView.makeTreeD3(featureModel.getRoot())+"}");
			}
			
		}else{
			System.out.println("FAIL");
			response.getWriter().append("FAIL");
		}   
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
}
