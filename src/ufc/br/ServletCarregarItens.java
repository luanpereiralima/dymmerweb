package ufc.br;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import br.ufc.lps.controller.xml.ControladorXml;
import br.ufc.lps.repository.SchemeXml;

/**
 * Servlet implementation class ServletCarregarItens
 */
@WebServlet("/carregarItens")
public class ServletCarregarItens extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ControladorXml controladorXml;
	private List<SchemeXml> listaItens;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletCarregarItens() {
        super();
        // TODO Auto-generated constructor stub
        controladorXml = new ControladorXml();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
        ServletContext contexto = getServletContext();
    	contexto.setAttribute("listaItens", controladorXml.getXml());
        
		Gson json = new Gson();
		response.getWriter().append(json.toJson(contexto.getAttribute("listaItens")));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
