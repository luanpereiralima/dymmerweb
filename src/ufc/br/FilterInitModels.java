package ufc.br;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import br.ufc.lps.controller.xml.ControladorXml;
import br.ufc.lps.model.xml.XMLFamiliarModel;
import br.ufc.lps.model.xml.XMLSplotModel;

/**
 * Servlet Filter implementation class FilterInitModels
 */
@WebFilter("/*")
public class FilterInitModels implements Filter {
    /**
     * Default constructor. 
     */
    public FilterInitModels() {
        // TODO Auto-generated constructor stub
    }

    private void initXMLmodels() {
		new XMLSplotModel();
		new XMLFamiliarModel();
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		ServletContext contexto = request.getServletContext();
        if(contexto.getAttribute("listaItens")==null){
        	contexto.setAttribute("listaItens", new ControladorXml().getXml());
        }
		
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
		initXMLmodels();
	}

}
