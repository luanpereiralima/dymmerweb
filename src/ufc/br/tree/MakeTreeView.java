package ufc.br.tree;

import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;

import com.google.gson.Gson;

import br.ufc.lps.model.visualization.d3.config.Children;
import br.ufc.lps.model.visualization.d3.config.Config;
import br.ufc.lps.splar.core.fm.FeatureTreeNode;

public class MakeTreeView {
	
	private static List<ChildrenTV> getTreeForRoot(ChildrenTV children, DefaultMutableTreeNode root){
	 	   
	 	   if(root.getChildCount() < 0)
	 		   return null;
	 			
	 	   List<ChildrenTV> lista = new ArrayList<>();
	 	   children.setNodes(lista);
	 	   
		   for(int i=0; i < root.getChildCount(); i++){
			   DefaultMutableTreeNode cr = (DefaultMutableTreeNode) root.getChildAt(i);
			   ChildrenTV c = new ChildrenTV();
			   c.setText(cr.toString());
			   FeatureTreeNode f = (FeatureTreeNode) cr;
			   c.setNodeId(f.getID());
			   c.setNodes(getTreeForRoot(c, cr));
			   lista.add(c);
		   }
		   return lista;
	    }
		
		public static String makeTree(DefaultMutableTreeNode node){
			ConfigTV config = new ConfigTV();	

			ChildrenTV chil = new ChildrenTV();
			FeatureTreeNode f = (FeatureTreeNode) node;
			
			chil.setText(node.toString());
			chil.setNodeId(f.getID());
			
			chil.setNodes(getTreeForRoot(chil, node));
			List<ChildrenTV> l = new ArrayList<>();
			l.add(chil);
			config.setRoot(l);
			
			Gson g = new Gson();
	 	    String saida = g.toJson(config);
	 	   
	 	    return saida;
		}
		
		private static List<Children> getTreeForRootD3(Children children, DefaultMutableTreeNode root){
		 	   
		 	   if(root.getChildCount() < 0)
		 		   return null;
		 			
		 	   List<Children> lista = new ArrayList<>();
		 	   children.setChildren(lista);
		 	   
			   for(int i=0; i < root.getChildCount(); i++){
				   DefaultMutableTreeNode cr = (DefaultMutableTreeNode) root.getChildAt(i);
				   Children c = new Children();
				   c.setName(cr.toString());
				   c.setChildren(getTreeForRootD3(c, cr));
				   lista.add(c);
			   }
			   return lista;
		    }
			
			public static String makeTreeD3(DefaultMutableTreeNode node){
				Config config = new Config();	

				Children chil = new Children();
				chil.setName(node.toString());
				
				chil.setChildren(getTreeForRootD3(chil, node));
				config.setRoot(chil);
				
				Gson g = new Gson();
		 	    String saida = g.toJson(config);
		 	   
		 	    return saida;
			}
		
		
}
