package ufc.br.tree;

import java.util.List;

public class ChildrenTV {
	private String text;
	private List<ChildrenTV> nodes;
	private String nodeId;
	
	public String getNodeId() {
		return nodeId;
	}
	
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	
	public List<ChildrenTV> getNodes() {
		return nodes;
	}
	
	public void setNodes(List<ChildrenTV> lista) {
		this.nodes = lista;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
}
