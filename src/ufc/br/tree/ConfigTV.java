package ufc.br.tree;

import java.util.List;

public class ConfigTV {
	private List<ChildrenTV> root;
	private String text;
	private String nodeId;
	
	public String getNodeId() {
		return nodeId;
	}
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}	
	public List<ChildrenTV> getRoot() {
		return root;
	}
	public void setRoot(List<ChildrenTV> root) {
		this.root = root;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
}
